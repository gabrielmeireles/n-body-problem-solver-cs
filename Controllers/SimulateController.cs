﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace n_body_problem_simulator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SimulateController : ControllerBase
    {
        private readonly ILogger<SimulateController> _logger;

        public SimulateController(ILogger<SimulateController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public SimulationResponseDTO Post([FromBody] SimulateDTO body)
        {
            SimulateService service = new SimulateService();
            return service.Simulate(body);
        }
    }
}
