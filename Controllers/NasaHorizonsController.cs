using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace n_body_problem_simulator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NasaHorizonsController : ControllerBase
    {
        private readonly ILogger<NasaHorizonsController> _logger;

        public NasaHorizonsController(ILogger<NasaHorizonsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public Dictionary<string, VectorXYZ> Get([FromQuery] int bodyId)
        {
            NasaHorizonsService service = new NasaHorizonsService();
            return service.GetEphemerities(bodyId);
        }
    }
}
