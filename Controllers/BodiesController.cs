﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace n_body_problem_simulator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BodiesController : ControllerBase
    {
        private readonly ILogger<BodiesController> _logger;

        public BodiesController(ILogger<BodiesController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public List<BodyDTO> Post([FromBody] BodyRequestDTO requestDTO)
        {
            BodiesService service = new BodiesService();
            return service.GetBodyParameters(requestDTO);
        }
    }
}
