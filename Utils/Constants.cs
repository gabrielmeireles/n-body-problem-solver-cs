namespace n_body_problem_simulator
{
    public static class Constants
    {
        public static readonly double protonMass = 1.672621777e-27; // kg
        public static readonly double electricPermittivity = 8.8541878176e-12; // F/m
        public static readonly double elementaryCharge = 1.602176565e-19; // C
        public static readonly double astronomicUnity = 1.496e11; // m
        public static readonly double electronMeanDensity = 7.3e-6; // m^-3
        public static readonly double solarWindMeanTemperature = 12 * elementaryCharge; //W.s
        public static readonly double solarWindSpeed = 400000; // m/s
        public static readonly double G = 6.6708E-11; // m³/(s.kg²)
    }
}