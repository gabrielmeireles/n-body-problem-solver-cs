using System.Collections.Generic;
namespace n_body_problem_simulator
{
    public class BodyRequestDTO
    {
        public List<int> BodyIds { get; set; }
        public long? SimulationDate { get; set; }
    }
}