using System.Runtime.CompilerServices;
using System.Collections.Generic;

namespace n_body_problem_simulator
{
    public class BodyDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double Mass { get; set; }
        public double Radius { get; set; }
        public string ReferenceBodyId { get; set; }
        public VectorXYZ CurrentPosition { get; set; }
        public VectorXYZ CurrentVelocity { get; set; }
        public VectorXYZ CurrentAcceleration { get; set; }
        public Trajectory BodyTrajectory { get; set; }

        public void StartTrajectory()
        {
            BodyTrajectory = new Trajectory();
            BodyTrajectory.AddVector(CurrentPosition);
        }
    }

    public class Trajectory
    {
        public Trajectory()
        {
            this.X = new List<double>();
            this.Y = new List<double>();
            this.Z = new List<double>();
        }
        public List<double> X { get; set; }
        public List<double> Y { get; set; }
        public List<double> Z { get; set; }

        public void AddVector(VectorXYZ vector)
        {
            this.X.Add(vector.X);
            this.Y.Add(vector.Y);
            this.Z.Add(vector.Z);
        }
    }
}
