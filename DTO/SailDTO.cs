using System.Collections.Generic;
namespace n_body_problem_simulator
{
    public class SailDTO : BodyDTO
    {
        public List<double> SpecificMechanicalEnergySun { get; set; }
        public List<double> SpecificMechanicalEnergyEarth { get; set; }
        public void InitializeMechanicalEnergyArrays() 
        {
            SpecificMechanicalEnergyEarth = new List<double>();
            SpecificMechanicalEnergySun = new List<double>();
        } 
    }
}