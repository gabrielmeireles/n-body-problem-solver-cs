namespace n_body_problem_simulator
{
    public class InputBodyDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Mass { get; set; }
        public double Radius { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }
        public double vx { get; set; }
        public double vy { get; set; }
        public double vz { get; set; }
        public int? ReferenceBodyId { get; set; }
    }
}
