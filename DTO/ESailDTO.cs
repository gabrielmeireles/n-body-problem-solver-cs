using System.Collections.Generic;

namespace n_body_problem_simulator
{
    public class ESailDTO : SailDTO
    {
        public int NumberWires { get; set; }
        public double WireRadius { get; set; }
        public double WireTension { get; set; }
        public double WireLength { get; set; }
        public ESailAttitudeDTO Attitude { get; set; }
    }

    public class ESailAttitudeDTO
    {
        public double Phi { get; set; }
        public double Theta { get; set; }
        public double Psi { get; set; }
    }
}