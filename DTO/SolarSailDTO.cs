namespace n_body_problem_simulator
{
    public class SolarSailDTO : SailDTO
    {
        public double Alpha { get; set; }
        public double Delta { get; set; }
        public double Area { get; set; }
        public double RDiff { get; set; }
        public double RSpec { get; set; }
        public double Ef { get; set; }
        public double Eb { get; set; }
        public double Af { get; set; }
        public double Ab { get; set; }
        public double ChiF { get; set; }
        public double ChiB { get; set; }
        public double Sigma => Mass / Area;
        public double Kappa => (Ef + Eb) == 0 ? 0 : (ChiF * Ef - ChiB * Eb) / (Ef + Eb);
    }
}