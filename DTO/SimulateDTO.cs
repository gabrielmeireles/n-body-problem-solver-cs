using System;
using System.Collections.Generic;

namespace n_body_problem_simulator
{
    public class SimulateDTO
    {
        public IEnumerable<BodyDTO> Bodies { get; set; }
        public IEnumerable<ESailDTO> ESails { get; set; }
        public IEnumerable<SolarSailDTO> SolarSails { get; set; }
        public int? NumberSteps { get; set; }
        public int? SimulationDays { get; set; }
    }
}
