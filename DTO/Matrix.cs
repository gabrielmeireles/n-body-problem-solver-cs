namespace n_body_problem_simulator
{
    public class Matrix
    {
        public double M11 { get; set; }
        public double M12 { get; set; }
        public double M13 { get; set; }
        public double M21 { get; set; }
        public double M22 { get; set; }
        public double M23 { get; set; }
        public double M31 { get; set; }
        public double M32 { get; set; }
        public double M33 { get; set; }

        public VectorXYZ RowX() => new VectorXYZ
        {
            X = M11,
            Y = M12,
            Z = M13,
        };

        public VectorXYZ RowY() => new VectorXYZ
        {
            X = M21,
            Y = M22,
            Z = M23,
        };

        public VectorXYZ RowZ() => new VectorXYZ
        {
            X = M31,
            Y = M32,
            Z = M33,
        };
    }
}