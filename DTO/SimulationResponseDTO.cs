using System.Collections;
using System.Collections.Generic;

namespace n_body_problem_simulator
{
    public class SimulationResponseDTO
    {
        public IEnumerable<BodyDTO> Planets { get; set; }
        public IEnumerable<ESailDTO> ESails { get; set; }
        public IEnumerable<SolarSailDTO> SolarSails { get; set; }
        public List<double> SimulationStepTimes { get; set; }
        public string Collision { get; set; }
    }
}