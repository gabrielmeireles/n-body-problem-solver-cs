using System;
using System.Diagnostics;
using System.Collections.Generic;
namespace n_body_problem_simulator
{
    public class NBodyProblemSolverService
    {
        public void SolveTrajectories(List<BodyDTO> inputBodies, int? numberSteps, int? simulationDays)
        {
            int finalNumberSteps = numberSteps ?? 10000;
            int finalSimulationDays = simulationDays ?? 365;

            double simulationTime = finalSimulationDays * 24 * 60 * 60;
            double timeStep = simulationTime / finalNumberSteps; 
            
            Integrator integrator = new Integrator(inputBodies, timeStep);

            double currentTime = 0;
            while(currentTime <= simulationTime)
            {
                integrator.Step();
                currentTime += timeStep;
            }
        }
    }
}