using System.Globalization;
using System;
using System.Threading;
using System.Collections.Generic;

namespace n_body_problem_simulator
{
    public class NasaHorizonsService
    {
        private readonly string dateFormat = "yyyy-MMM-dd HH:mm";
        public Dictionary<string, VectorXYZ> GetEphemerities(int bodyId, long? simulationDate = null)
        {
            DateTime simulationStartDate;
            
            if (simulationDate.HasValue)
            {
                simulationStartDate = Methods.UnixTimeToDateTime(simulationDate.Value);
            } else
            {
               simulationStartDate = DateTime.Now;
            };
            
            DateTime simulationEndDate = simulationStartDate.AddDays(1);

            TelnetConnection tc = new TelnetConnection("horizons.jpl.nasa.gov", 6775);
            Thread.Sleep(2000);
            WriteTelnetInput(tc, bodyId.ToString());
            WriteTelnetInput(tc, "e");
            WriteTelnetInput(tc, "v");
            WriteTelnetInput(tc, "@0");
            WriteTelnetInput(tc, "eclip");
            WriteTelnetInput(tc, GetDateString(simulationStartDate));
            WriteTelnetInput(tc, GetDateString(simulationEndDate));
            WriteTelnetInput(tc, "1d");
            WriteTelnetInput(tc, "n");
            WriteTelnetInput(tc, "J2000");
            WriteTelnetInput(tc, "1");
            WriteTelnetInput(tc, "1");
            WriteTelnetInput(tc, "YES");
            WriteTelnetInput(tc, "YES");
            WriteFinalTelnetInput(tc, "2");
            Thread.Sleep(2000);

            string response = tc.Read();
            response = response.Split("$$SOE")[1];
            response = response.Split("$$EOE")[0];
            response = response.Split("\\r\\n")[0];

            string[] fields = response.Split(",");
            var x = ParseTelnetDouble(fields[3]);
            var y = ParseTelnetDouble(fields[4]);
            var z = ParseTelnetDouble(fields[5]);
            var vx = ParseTelnetDouble(fields[6]);
            var vy = ParseTelnetDouble(fields[7]);
            var vz = ParseTelnetDouble(fields[8]);

            var ephemerities = new Dictionary<string, VectorXYZ>();
            var position = new VectorXYZ(x, y, z);
            var velocity = new VectorXYZ(vx, vy, vz);

            ephemerities.Add("position", position);
            ephemerities.Add("velocity", velocity);

            return ephemerities;
        }

        private double ParseTelnetDouble(string input)
        {
            string fixedString = input.Trim();
            return Double.Parse(fixedString, System.Globalization.NumberStyles.Float, CultureInfo.CreateSpecificCulture("en-US"));
        }

        private void WriteTelnetInput(TelnetConnection tc, string input)
        {
            Thread.Sleep(500);
            tc.Read();
            tc.WriteLine(input);
            tc.Read();
        }

        private void WriteFinalTelnetInput(TelnetConnection tc, string input)
        {
            Thread.Sleep(500);
            tc.Read();
            tc.WriteLine(input);
        }

        private string GetDateString(DateTime date)
        {
            return date.ToString(dateFormat, CultureInfo.CreateSpecificCulture("en-US"));
        }
    }
}
