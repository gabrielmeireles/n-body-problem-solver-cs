using System.Numerics;
using System;
using System.Collections.Generic;

namespace n_body_problem_simulator
{
    public class Integrator
    {
        private List<BodyDTO> _bodies;
        private BodyDTO _earth;
        private BodyDTO _sun;
        private double _timeStep;
        private double G = Constants.G;

        public Integrator(List<BodyDTO> bodies, double timeStep)
        {
            _bodies = bodies;
            _timeStep = timeStep;
            _earth = bodies.Find(body => body.Id.Equals("399"));
            _sun = bodies.Find(body => body.Id.Equals("0"));
        }
        public void Step()
        {
            foreach(BodyDTO body in _bodies)
            {
                ComputeAcceleration(body);
                UpdateLocations(body);
                if (body is ESailDTO || body is SolarSailDTO)
                {
                    CalculateMechanicalSpecificEnergy((SailDTO) body);
                }
            }
        }

        private void ComputeAcceleration(BodyDTO body)
        {
                VectorXYZ acceleration = IntegrateAccelerationRK4(body);
                body.CurrentAcceleration = acceleration;
        }

        private void UpdateLocations(BodyDTO body)
        {
                body.CurrentVelocity = body.CurrentVelocity + body.CurrentAcceleration * _timeStep;
                body.CurrentPosition = body.CurrentPosition + body.CurrentVelocity * _timeStep;
                body.BodyTrajectory.AddVector(body.CurrentPosition);
        }

        private void CalculateMechanicalSpecificEnergy(SailDTO sail)
        {
            if (_earth is not null) {
                double specificEnergyEarth = Methods.CalculateSpecificMechanicalEnergy(sail, _earth);
                sail.SpecificMechanicalEnergyEarth.Add(specificEnergyEarth);
            }

            if (_sun is not null) {
                double specificEnergySun = _sun is null ? 0 : Methods.CalculateSpecificMechanicalEnergy(sail, _sun);
                sail.SpecificMechanicalEnergySun.Add(specificEnergySun);
            }
        }

        private VectorXYZ IntegrateAccelerationRK4(BodyDTO body)
        {
            VectorXYZ tempVelocity = body.CurrentVelocity;
            VectorXYZ tempPosition = body.CurrentPosition;

            VectorXYZ accelerationK1 = CalculateK1(body);
            VectorXYZ accelerationK2 = CalculateK2(body, accelerationK1, tempPosition, tempVelocity);
            VectorXYZ accelerationK3 = CalculateK3(body, accelerationK2, tempPosition, tempVelocity);
            VectorXYZ accelerationK4 = CalculateK4(body, accelerationK3, tempPosition, tempVelocity);

            return (accelerationK1 + accelerationK2 * 2 + accelerationK3 * 2 + accelerationK4) / 6;
        }

        private VectorXYZ CalculateBaseAcceleration(BodyDTO targetBody, VectorXYZ position, VectorXYZ velocity)
        {
            VectorXYZ finalAcceleration = new VectorXYZ(0,0,0);
            foreach (BodyDTO externalBody in _bodies)
            {
                if (targetBody.Id != externalBody.Id && externalBody is not SailDTO)
                {
                    VectorXYZ distance = externalBody.CurrentPosition - position;
                    if (distance.Norm() < externalBody.Radius + targetBody.Radius)
                    {
                        throw new CollisionException(targetBody.Name, externalBody.Name);
                    }
                    VectorXYZ gravitationalAcceleration = distance * (G *1e-9 * externalBody.Mass / Math.Pow(distance.Norm(), 3));
                    finalAcceleration += gravitationalAcceleration;
                }
            }

            if (targetBody is ESailDTO)
            {
                VectorXYZ eSailAcceleration = ESailCalculator.CalculateESailAcceleration((ESailDTO) targetBody, position);
                finalAcceleration += eSailAcceleration;
            }

            if (targetBody is SolarSailDTO)
            {
                VectorXYZ solarSailAcceleration = SolarSailCalculator.CalculateSolarSailAcceleration((SolarSailDTO) targetBody, position, velocity, _sun.Mass);
                finalAcceleration += solarSailAcceleration;
            }

            return finalAcceleration;
        }

        private VectorXYZ CalculateK1(BodyDTO body)
        {
            return CalculateBaseAcceleration(body, body.CurrentPosition, body.CurrentVelocity);
        }

        private VectorXYZ CalculateK2(BodyDTO body, VectorXYZ accelerationK1, VectorXYZ position, VectorXYZ velocity)
        {
            velocity = PartialStep(velocity, accelerationK1, 0.5 * _timeStep);
            position = PartialStep(position, velocity, 0.5 * _timeStep);
            return CalculateBaseAcceleration(body, position, velocity);
        }

        private VectorXYZ CalculateK3(BodyDTO body, VectorXYZ accelerationK2, VectorXYZ position, VectorXYZ velocity)
        {
            velocity = PartialStep(velocity, accelerationK2, 0.5 * _timeStep);
            position = PartialStep(position, velocity, 0.5 * _timeStep);
            return CalculateBaseAcceleration(body, position, velocity);
        }

        private VectorXYZ CalculateK4(BodyDTO body, VectorXYZ accelerationK3, VectorXYZ position, VectorXYZ velocity)
        {
            velocity = PartialStep(velocity, accelerationK3, _timeStep);
            position = PartialStep(position, velocity, _timeStep);
            return CalculateBaseAcceleration(body, position, velocity);
        }

        private VectorXYZ PartialStep(VectorXYZ vector1, VectorXYZ vector2, double partialStepTime)
        {
            return vector1 + vector2 * partialStepTime;
        }
    }
}