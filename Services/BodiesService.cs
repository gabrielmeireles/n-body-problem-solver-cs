using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace n_body_problem_simulator
{
    public class BodiesService
    {
        private NasaHorizonsService horizons = new NasaHorizonsService();

        private Dictionary<string, InputBodyDTO> _horizonBodies;
        public List<BodyDTO> GetBodyParameters(BodyRequestDTO requestDTO)
        {
            StreamReader r = new StreamReader("./Resources/bodies.json");
            string json = r.ReadToEnd();
            _horizonBodies = JsonConvert.DeserializeObject<Dictionary<string, InputBodyDTO>>(json);
            
            List<BodyDTO> listBodies = new List<BodyDTO>();

            Parallel.ForEach(requestDTO.BodyIds, bodyId =>
            {
                FillBodyParameters(bodyId, listBodies, requestDTO.SimulationDate);
            });

            return listBodies;
        }

        private void FillBodyParameters(int bodyId, List<BodyDTO> listBodies, long? simulationDate)
        {
            InputBodyDTO body = _horizonBodies[bodyId.ToString()];

            if (!body.Id.Equals(0)) {
                Dictionary<string, VectorXYZ> ephemerities = horizons.GetEphemerities(bodyId, simulationDate);

                VectorXYZ position = ephemerities["position"];
                VectorXYZ velocity = ephemerities["velocity"];

                body.x = position.X;
                body.y = position.Y;
                body.z = position.Z;

                body.vx = velocity.X;
                body.vy = velocity.Y;
                body.vz = velocity.Z;
            }

            listBodies.Add(GetBodyDTOFromInput(body));
        }

        private BodyDTO GetBodyDTOFromInput(InputBodyDTO input)
        {
            BodyDTO body = new BodyDTO();
            body.Id = input.Id.ToString();
            body.Name = input.Name;
            body.Mass = input.Mass;
            body.Radius = input.Radius;
            body.ReferenceBodyId = input.ReferenceBodyId.ToString();
            body.CurrentPosition = new VectorXYZ(input.x, input.y, input.z);
            body.CurrentVelocity = new VectorXYZ(input.vx, input.vy, input.vz);

            return body;
        }
    }
}