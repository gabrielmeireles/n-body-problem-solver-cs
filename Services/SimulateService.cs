using System.Linq;
using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace n_body_problem_simulator
{
    public class SimulateService
    {
        private NasaHorizonsService horizons = new NasaHorizonsService();
        private NBodyProblemSolverService solver = new NBodyProblemSolverService();
        public SimulationResponseDTO Simulate(SimulateDTO simulationParameters)
        {
            List<BodyDTO> listBodies = CreateBodiesList(simulationParameters);

            string collision = null;
            try {
                solver.SolveTrajectories(listBodies, simulationParameters.NumberSteps, simulationParameters.SimulationDays);
            } catch (CollisionException e) {
                collision = e.Message;
            }
            ReduceTrajectories(listBodies, simulationParameters.NumberSteps);

            return new SimulationResponseDTO
            {
                ESails = listBodies.FindAll(body => body is ESailDTO).Select(body => (ESailDTO) body),
                SolarSails = listBodies.FindAll(body => body is SolarSailDTO).Select(body => (SolarSailDTO) body),
                Planets = listBodies.FindAll(body => body is not ESailDTO && body is not SolarSailDTO),
                Collision = collision
            };
        }

        private List<BodyDTO> CreateBodiesList(SimulateDTO simulationParameters)
        {
            foreach (BodyDTO bodyDTO in simulationParameters.Bodies)
            {
                AddReferenceBodyPosition(simulationParameters.Bodies, bodyDTO);
                bodyDTO.StartTrajectory();
            }

            List<SailDTO> sails = new List<SailDTO>();
            sails.AddRange(simulationParameters.ESails);
            sails.AddRange(simulationParameters.SolarSails);

            foreach (SailDTO sail in sails)
            {
                AddReferenceBodyPosition(simulationParameters.Bodies, sail);
                sail.StartTrajectory();
                sail.InitializeMechanicalEnergyArrays();
            };

            List<BodyDTO> listBodies = new List<BodyDTO>();
            listBodies.AddRange(simulationParameters.Bodies);
            listBodies.AddRange(sails);

            return listBodies;
        }

        private void AddReferenceBodyPosition(IEnumerable<BodyDTO> listBodies, BodyDTO bodyDTO)
        {
            if (bodyDTO.ReferenceBodyId != null)
            {
                foreach (BodyDTO referenceBody in listBodies)
                {
                    if (referenceBody.Id == bodyDTO.ReferenceBodyId)
                    {   
                        if (referenceBody.ReferenceBodyId != null) {
                            AddReferenceBodyPosition(listBodies, referenceBody);
                        }

                        bodyDTO.CurrentPosition += referenceBody.CurrentPosition;
                        bodyDTO.CurrentVelocity += referenceBody.CurrentVelocity;
                        bodyDTO.ReferenceBodyId = null;
                        break;
                    }
                }
            }
        }

        private void ReduceTrajectories(List<BodyDTO> listBodies, int? steps)
        {
            int reductionSize = steps.HasValue ? steps.Value / 5000 : 2;
            foreach(BodyDTO body in listBodies)
            {
                body.BodyTrajectory.X = Methods.ReduceArray(body.BodyTrajectory.X, reductionSize);
                body.BodyTrajectory.Y = Methods.ReduceArray(body.BodyTrajectory.Y, reductionSize);
                body.BodyTrajectory.Z = Methods.ReduceArray(body.BodyTrajectory.Z, reductionSize);

                if (body is SailDTO) {
                   SailDTO sail = (SailDTO) body;
                   sail.SpecificMechanicalEnergyEarth = Methods.ReduceArray(sail.SpecificMechanicalEnergyEarth, reductionSize);
                   sail.SpecificMechanicalEnergySun = Methods.ReduceArray(sail.SpecificMechanicalEnergySun, reductionSize);
                }
            }
        }
    }
}