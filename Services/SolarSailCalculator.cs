using System.IO.Compression;
using System;

namespace n_body_problem_simulator
{
    public class SolarSailCalculator
    {
        private static readonly double SigmaC = 1.5368e-3;

        private static double Cos (double d) => Math.Cos(Math.PI * d / 180.0);
        private static double Sin (double d) => Math.Sin(Math.PI * d / 180.0);
        private static double Cos2 (double d) => Math.Pow(Math.Cos(Math.PI * d / 180.0), 2);
        private static double Cos3 (double d) => Math.Pow(Math.Cos(Math.PI * d / 180.0), 3);
        
        public static VectorXYZ CalculateSolarSailAcceleration(SolarSailDTO sail, VectorXYZ basePosition, VectorXYZ baseVelocity, double sunMass)
        {
            VectorXYZ lightnessVector = CalculateLightnessVector(sail);
            return CalculateAcceleration(lightnessVector, basePosition, baseVelocity, sunMass);
        }

        private static VectorXYZ CalculateLightnessVector(SolarSailDTO sail)
        {
            double alpha = sail.Alpha;
            double delta = sail.Delta;
            double loadRatio = SigmaC / (2 * sail.Sigma);

            double lx = 2 * sail.RSpec * Cos3(delta) * Cos3(alpha) 
            + (sail.ChiF * sail.RDiff + sail.Kappa * sail.Af) * Cos2(delta) * Cos2(alpha)
            + (sail.Af + sail.RDiff) * Cos(delta) * Cos(alpha);

            double ly = 2 * sail.RSpec * Cos3(delta) * Cos2(alpha) * Sin(alpha)
            + (sail.ChiF * sail.RDiff + sail.Kappa * sail.Af) * Cos2(delta) * Cos(alpha) * Sin(alpha);

            double lz = 2 * sail.RSpec * Cos2(delta) * Cos2(alpha) * Sin(delta)
            + (sail.ChiF * sail.RDiff + sail.Kappa * sail.Af) * Cos(delta) * Cos(alpha) * Sin(delta);

            return new VectorXYZ(lx, ly, lz) * loadRatio;
        }

        private static VectorXYZ CalculateAcceleration(VectorXYZ lightnessVector, VectorXYZ basePosition, VectorXYZ baseVelocity, double sunMass)
        {
            VectorXYZ r = basePosition;
            VectorXYZ v = baseVelocity;
            VectorXYZ h = Methods.vectorialProduct(r, v);

            double R = r.Norm();
            double H = h.Norm();

            double lx = lightnessVector.X;
            double ly = lightnessVector.Y;
            double lz = lightnessVector.Z;

            double ax = (lx * r.X / R)
            + (ly / (H * R)) * (r.Z * (r.Z * v.X - r.X * v.Z) - r.Y * (r.X * v.Y - r.Y - v.X))
            + (lz / H) * (r.Y * v.Z - r.Z * v.Y);

            double ay = (lx * r.Y / R)
            + (ly / (H * R)) * (r.X * (r.X * v.Y - r.Y * v.X) - r.Z * (r.Y * v.Z - r.Z - v.Y))
            + (lz / H) * (r.Z * v.X - r.X * v.Z);

            double az = (lx * r.Z / R)
            + (ly / (H * R)) * (r.Y * (r.Y * v.Z - r.Z * v.Y) - r.X * (r.Z * v.X - r.X - v.Z))
            + (lz / H) * (r.X * v.Y - r.Y * v.X);

            return new VectorXYZ(ax, ay, az) * (sunMass * Constants.G * 1e-9 / Math.Pow(R, 2));
        }
    }
}