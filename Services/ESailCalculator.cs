using System;

namespace n_body_problem_simulator
{
    public class ESailCalculator
    {
        private static double Cos (double d) => Math.Cos(Math.PI * d / 180.0);
        private static double Sin (double d) => Math.Sin(Math.PI * d / 180.0);
        private static double Cos2 (double d) => Math.Pow(Math.Cos(Math.PI * d / 180.0), 2);
        private static double Sin2 (double d) => Math.Pow(Math.Sin(Math.PI * d / 180.0), 2);
        public static VectorXYZ CalculateESailAcceleration(ESailDTO sail, VectorXYZ basePosition)
        {
            VectorXYZ position = basePosition * 1000;
            double basePropulsiveForce = CalculateBasePropulsiveForce(sail);
            VectorXYZ accelerationVector = CalculateAccelerationVector(sail, basePropulsiveForce, position);
            return accelerationVector * 1000;
        }
        private static double CalculateBasePropulsiveForce(ESailDTO sail)
        {
            double numerator = 6.18 * Constants.protonMass * Math.Pow(Constants.solarWindSpeed, 2) *
                Math.Sqrt(Constants.electronMeanDensity * Constants.electricPermittivity * Constants.solarWindMeanTemperature);

            double denominatorLn = (2 / sail.WireRadius) * 
                Math.Sqrt(Constants.electricPermittivity * Constants.solarWindMeanTemperature 
                    / (Constants.electronMeanDensity * Math.Pow(Constants.elementaryCharge, 2)));

            double denominatorExp = (Constants.protonMass * Math.Pow(Constants.solarWindSpeed, 2) 
                / (Constants.elementaryCharge * sail.WireTension)) *
                Math.Log(denominatorLn);
            
            double denominator = Constants.elementaryCharge * Math.Sqrt(Math.Exp(denominatorExp) - 1);

            return numerator/denominator;
        }
        private static VectorXYZ CalculateAccelerationVector(ESailDTO sail, double basePropulsiveForce, VectorXYZ position)
        {
            VectorXYZ attitudeAnglesVector = CalculateAtitudeAnglesVector(sail.Attitude);
            VectorXYZ bodyThrust = CalculateBodyThrust(attitudeAnglesVector, basePropulsiveForce, sail, position);
            Matrix transformationMatrix = CalculateTransformationMatrix(sail.Attitude);

            VectorXYZ orbitThrust = transformationMatrix * bodyThrust;

            Matrix rotationMatrix = CalculateRotationMatrix(position);
            VectorXYZ inertialReferenceThrust = rotationMatrix * orbitThrust;

            VectorXYZ acceleration = inertialReferenceThrust / sail.Mass;

            return acceleration;
        }

        private static Matrix CalculateRotationMatrix(VectorXYZ position)
        {
            VectorXYZ kInertial = new VectorXYZ(0, 0, 1);

            VectorXYZ versorPosition = position / position.Norm();
            VectorXYZ vectorialProduct = Methods.vectorialProduct(kInertial, versorPosition);

            VectorXYZ ki = versorPosition;
            VectorXYZ ji = vectorialProduct / vectorialProduct.Norm();
            VectorXYZ ii = Methods.vectorialProduct(ji, ki);

            return new Matrix
            {
                M11 = ii.X,
                M12 = ji.X,
                M13 = ki.X,
                M21 = ii.Y,
                M22 = ji.Y,
                M23 = ki.Y,
                M31 = ii.Z,
                M32 = ji.Z,
                M33 = ki.Z,
            };
        }

        private static VectorXYZ CalculateBodyThrust(VectorXYZ attitudeAnglesVector, double basePropulsiveForce, ESailDTO sail, VectorXYZ position)
        {
            return attitudeAnglesVector * 0.5 * sail.NumberWires * sail.WireLength * basePropulsiveForce 
                    * Math.Pow((Constants.astronomicUnity/(position.Norm())), (7/6));
        }

        private static VectorXYZ CalculateAtitudeAnglesVector(ESailAttitudeDTO attitude)
        {
            double theta = attitude.Theta;
            double phi = attitude.Phi;

            return new VectorXYZ
            {
                X = Cos(phi) * Sin(theta) * Cos(theta),
                Y = (-1) * Sin(phi) * Cos(phi) * Cos2(theta),
                Z = Cos2(phi) * Cos2(theta) + 1,
            };
        }

        private static Matrix CalculateTransformationMatrix(ESailAttitudeDTO attitude)
        {
            double psi = attitude.Psi;
            double theta = attitude.Theta;
            double phi = attitude.Phi;

            return new Matrix
            {
                M11 = Cos(psi) * Cos(theta),
                M12 = Cos(psi) * Sin(theta) * Sin(phi) + Sin(psi) * Cos(phi),
                M13 = (-1) * Cos(psi) * Sin(theta) * Cos(phi) + Sin(psi) * Sin(phi),
                M21 = (-1) * Sin(psi) * Cos(theta),
                M22 = (-1) * Sin(psi) * Sin(theta) * Sin(phi) + Cos(psi) * Cos(phi),
                M23 = Sin(psi) * Sin(theta) * Cos(phi) + Cos(psi) * Sin(phi),
                M31 = Sin(theta),
                M32 = (-1) * Cos(theta) * Sin(phi),
                M33 = Cos(theta) * Cos(phi),
            };
        }
    }
}